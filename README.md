# About

gas (Go-As-Service) is a windows-service (winsvc) helper, wrapping arbitrary executables so they can be set up as windows services. This includes any kind of application, including script interpreters.

The primary goal is simplicity and ease of use, which is why the availaible install options ignore many of the settings possible in the windows ServiceCOntrolManager (SCM). If such features are required, they can be set in the SCM interface after the fact. Setup or Removal of a service is done in a single command. The CLI interface makes gas suitable for use in install scripts. It provides facilities for log-wrapping, allowing service applications to simply log to stdout.


# Usage Basics

`gas install [options...] <service-name> <exec-path> [params...]` installs the executable with the set params as a service. See [options](#install-options) for an explanation of available options.

`gas remove <service-name> remove` removes the service.

`gas start|stop <service-name> start|stop` starts or stops the service


# Install Options

`-desc <description>`\
Set the service description

`-dir <path>`\
Set the programs working directory to path. Defaults to the programs base-path

`-stdout <path`\
`-stderr <path>`\
Redirect the programs stdout/err to the file at path. The service start will fail if path is not accessible.

`-log <path>`\
Write a wrapper log to path. Defaults to no log. The service start will fail if path is not accessible.

`-logsvc`\
Redirect the programs stdout/err to the wrapper log. `-log` must be set for this to take effect. Ignores `-stdout` & `-stderr`.


# Note

Currently, `gas` will not check if `exec-path` is actually an executable or not, only if it is a file.


# Example

Compile `cmd/echosrv.go`.

`gas -n Echo -desc "An http echo server" -logsvc -log svc.log install echosrv.exe -p 4200 -t "hello world!"`

`gas -n Echo start` (or use the SCM GUI). A logfile `svc.log` should be created in your current directory.

Open your browser and goto `localhost:4200`.


# Todo

Stop needs to try and stop the process gracefully (send Ctrl-Break) using `syscall` and `kernel32.dll` "GenerateCOnsoleCtrlEvent"
as of now the process is simply killed off.

`-depends <name>[,name...]` set dependencies for the service as a comma separated list of service names

`gas -n <service-name> status`
