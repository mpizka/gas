//go:build windows
//+build windows

package main

import (
    "os"
    "os/exec"
    "path/filepath"
    "fmt"
    "golang.org/x/sys/windows/svc"
    "golang.org/x/sys/windows/svc/mgr"
    "log"
)

// return dir path
func getDirPath(p string) (string, error) {
    p, err := filepath.Abs(prog)
    if err != nil {
        return "", err
    }
    fi, err := os.Stat(p)
    if err != nil {
        return "", err
    }
    if ! fi.Mode().IsDir() {
        return "", fmt.Errorf("%s is no directory", p)
    }
    return p, nil
}

// return file path
func getFilePath(p string) (string, error) {
    p, err := filepath.Abs(prog)
    if err != nil {
        return "", err
    }
    fi, err := os.Stat(p)
    if err != nil {
        return "", err
    }
    if fi.Mode().IsDir() {
        return "", fmt.Errorf("%s is a directory", p)
    }
    return p, nil
}


// returns path of current executable
func getExecPath() (string, error) {
    prog := os.Args[0]

    p, err := filepath.Abs(prog)
    if err != nil {
        return "", err
    }

    // correct: windows allows invoc. wo. extension
    if filepath.Ext(p) == "" {
        p += ".exe"
    }

    fi, err := os.Stat(p)
    if err != nil {
        return "", err
    }

    if fi.Mode().IsDir() {
        return "", fmt.Errorf("%s is a directory", p)
    }

    return p, nil
}


// setup the wrapped command
func getCmd(args []string) (*exec.Command, error) {
    cmd := exec.Command(args[0], args[1:]...)
    // set working directory
    if svcDir != "" {
        dir, err := getDirPath(svcDir)
        if err != nil {
            return nil, err
        }
        cmd.Dir = dir
    }
    
    // set stdout & stderr path redirections
    if !svcLog && svcStdoutPath != "" {
        fh, err := os.OpenFile(svcStdoutPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
        if err != nil {
            return nil, err
        }
        cmd.Stdout = fh
    }
    if !svcLog && svcStderrPath != "" {
        fh, err := os.OpenFile(svcStderrPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
        if err != nil {
            return nil, err
        }
        cmd.Stderr = fh
    }

    return cmd, nil
}


// represents the service
// only the impl. of Execute is actually relevant
type service struct{}

// Execute is the implementation of a windows service, called by
// svc.Run(name, service). it starts the actual service as a goroutine,
// recvs & processes svc change req. from r
// and sends status updates to changes
// ssec informs svc manager if exit code is service specific
// errno is an error return with 0 being "no error"
func (s *service) Execute(args []string, r <-chan svc.ChangeRequest, changes chan<- svc.Status) (ssec bool, errno uint32) {

    const cmdsAccepted = svc.AcceptStop | svc.AcceptShutdown

    // signal service starting
    changes <- svc.Status{State: svc.StartPending}

    // setup logging
    fh = os.Stdout
    if logPath != "" {
        fh, err := os.OpenFile(logPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
        if err != nil {
            log.Printf("wrapper: log: %s", err)
            return false, 1
        }
        defer fh.Close()
        log.SetOutput(fh)
    }

    // setup wrapped command
    svcCmd, err = getCmd(insOpt.Args())
    if err != nil {
        log.Printf("wrapper: setup cmd: %s", err)
    }
    if svcLog {
        svcCmd.Stdout = fh
        svcCmd.Stderr = fh
    }

    // run command
    cmdError := make(chan error, 1)
    go func() {
        if err := svcCmd.Run(); err != nil {
            cmdError <- err
        }
    }()

    // signal service Running
    changes <- svc.Status{State: svc.Running, Accepts: cmdsAccepted}

loop:
    for {
        select {
        // received svc change request
        case c := <-r:
            switch c.Cmd {
            case svc.Interrogate:
                changes <- c.CurrentStatus
            case svc.Stop, svc.Shutdown:
                log.Println("wrapper: recv stop/shutdown")
                // shutdown running process
                svcCmd.Process.Kill()
                break loop
            }
        case e := <-cmdError:
            log.Printf("wrapper: service error: %s", e)
            break loop
        }
    }

    // signal stop to svc manager
    changes <- svc.Status{State: svc.StopPending}
    return false, 0
}


// install svc
func installService(name, dispName, desc string, args ...string) error {
    // connect to svc-mgr
    m, err := mgr.Connect()
    if err != nil {
        return err
    }
    defer m.Disconnect()

    // test service exists
    s, err := m.OpenService(name)
    if err == nil {
        s.Close()
        return fmt.Errorf("install: svc '%s' already exists", name)
    }

    // create service
    path, err := getExecPath()
    if err != nil {
        return err
    }
    s, err = m.CreateService(
        name,
        path,
        mgr.Config{DisplayName: dispName, Description: desc},
        args...,
    )
    if err != nil {
        return err
    }
    defer s.Close()

    return nil
}


// remove installed windows service
func removeService(name string) error {
    // connect to svc-mgr
    m, err := mgr.Connect()
    if err != nil {
        return err
    }
    defer m.Disconnect()

    // test service exists
    s, err := m.OpenService(name)
    if err != nil {
        return fmt.Errorf("remove: no such svc")
    }
    defer s.Close()

    // delete service
    err = s.Delete()
    if err != nil {
        return err
    }

    return nil
}

// start service
func startService(name string) error {
    m, err := mgr.Connect()
    if err != nil {
        return err
    }
    defer m.Disconnect()

    // test service exists
    s, err := m.OpenService(name)
    if err != nil {
        return fmt.Errorf("remove: no such svc")
    }
    defer s.Close()

    err := s.Start()
    if err != nil {
        return fmt.Errorf("start: could not start svc: %s", err)
    }

    return nil
}

// stop service
func stopService(name string) error {
    m, err := mgr.Connect()
    if err != nil {
        return err
    }
    defer m.Disconnect()

    // test service exists
    s, err := m.OpenService(name)
    if err != nil {
        return fmt.Errorf("remove: no such svc: '%s'", name)
    }
    defer s.Close()

    err := s.Control(svc.Stop)
    if err != nil {
        return fmt.Errorf("stop: could not stop svc: %s", err)
    }

    return nil
}
