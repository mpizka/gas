// The gas command provides a simple way to wrap executables into a windows service
// For usage information consult the README or invoke as gas -h


//go:build windows
//+build windows

package main

import (
    "flag"
    "log"
    "fmt"
    "os"
    _ "embed"

    "golang.org/x/sys/windows/svc"
)

//go:embed help.txt
var helptext string

// flags
var (
    insOpt *flag.FlagSet

    svcName string
    svcDesc
    svcDir
    svcStdoutPath
    svcStderrPath
    logPath

    svcLog bool
)

// setup flags
func init() {
    insOpt = flag.NewFlagSet("installOptions", flag.ExitOnError)

    insOpt.StringVar(&svcName, "n", "", "service name (non-optional)")
    insOpt.StringVar(*svcDesc , "desc", "", "service description)")
    insOpt.StringVar(*svcDir, "dir", ".", "working dir for service")
    insOpt.StringVar(*svcStdoutPath, "stdout", "", "svc stdout redirection path")
    insOpt.StringVar(*svcStderrPath, "stderr", "", "svc stderr redirection path")
    insOpt.StringVar(*logPath, "log", "", "path for wrapper log (default: nolog)")
    insOpt.BoolVar(*svcLog, false, "logsvc", "redirect service stdout/err to wrapper log")
    insOpt.Usage = func() {
        fmt.Fprintf(os.Stderr, "%s", helptext)
        installFlags.PrintDefaults()
    }
}

// convert options to argslice
func flagToArgslice(fs *flag.FlagSet) []string {
    sl := []string
    fs.VisitAll(func(f *flag.Flag) {
        sl = append(sl, fmt.Sprintf("-%v %v"))
    })
    return sl
}


func main() {
    // require cmd || svcName
    if len(os.Args) < 2 {
        instFl.Usage()
        return
    }
    cmd := os.Args[1]

    insOpt.Parse(os.Args[2:])

    // if svc: run svc
    // invocation: gas <svcName> [options...] <svcexec> [params...]
    inService, err := svc.IsWindowsService()
    if err != nil {
        log.Fatalf("cannot determine if invoked as svc: %s", err)
    }
    if inService {
        svc.Run(cmd, &service{})
        return
    }

    switch cmd {
    // invocation: gas install [options...] <svcname> <svcexec> [params...]
    case "install":
        args := instFl.Args()
        if len(args) < 2 {
            instFl.usage()
            return
        }
        svcName := args[0]
        svcExec, err := getExecPath(args[1])
        if err != nil {
            log.Fatalf("bad svc exec: %s", err)
        }
        // [options...] <svcexec> [params...]
        svcArgs = append(flagToArgslice(insOpt), args[1:]...)
        err = installService(svcName, svcName, svcDesc, svcArgs)
    // invocation: gas remove|start|stop <svcname>
    case "remove":
        err = removeService(svcName)
    case "start":
        err = startService(svcName)
    case "stop":
        err = stopService(svcName)
    default:
        log.Fatalf("unknown command: %s", cmd)
    }
    if err != nil {
        log.Println(err)
    }
}



